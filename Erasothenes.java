import java.util.ArrayList;

public class Erasothenes {
    public static void main(String[] args) {
        int n = 1000000;
        
        ArrayList<Integer> nums = new ArrayList<Integer>();
        for (int x = 2; x <= n; x++)
            nums.add(x);
        ArrayList<Integer> nums2 = new ArrayList<Integer>();
        nums2.addAll(nums);
        
        for (int x = 0; x < nums.size(); x++) {
            for (int y = x+1; y < nums.size(); y++) {
                if (nums.get(y) % nums.get(x) == 0)
                    nums2.set(y, null);
            }
            System.out.println(x);
        }

        for (int x = 0; x < nums2.size(); x++) {
            if (nums2.get(x) == null) {
                nums2.remove(x);
                x--;
            }
        }
        
        System.out.println(nums2.size());
    }
}